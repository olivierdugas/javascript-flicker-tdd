'use strict';
global.sinon = require("sinon");
var chai = require('chai')
	, assert = chai.assert
	, expect = chai.expect
	, should = chai.should();

var FlickrFetcher = require('./flickr-fetcher.js');

var photo = {
	farm: '1',
	server: '2',
	id: '1418878',
	secret: '1e92283336',
	size: 'm',
	title: 'A photo title'
	};

var photoURL = 'https://farm1.staticflickr.com/2/1418878_1e92283336_m.jpg';

var anotherPhoto = {
	farm: '2',
	server: '3',
	id: '2418878',
	secret: '2e92283336',
	size: 'z',
	title: 'Another photo title'
	};

var anotherPhotoURL = 'https://farm2.staticflickr.com/3/2418878_2e92283336_z.jpg';

var apiKey = 'does not matter much what this is right now';

var fakeData = {
	'photos': {
		'page':    1,
		'pages':   2872,
		'perpage': 100,
		'total':   '287170',
		'photo':   [ photo, anotherPhoto ]
		}
	};

var fakeFetcher = sinon.stub();
var expectedFetchURL = ['https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=',
						apiKey,
						'&text=pugs&format=json&nojsoncallback=1'].join('');

beforeEach(function() {
	fakeFetcher.reset();
});

describe('#photoToURL()', function() {
	it('should take a photo object from Flickr and return a url', function() {
		var expected = photoURL;
		var actual = FlickrFetcher.photoToURL(photo);
		actual.should.equal(expected);
	});
});

describe('#transformPhoto()', function() {
	it('should take a photo and return and object with just a title and the url', function() {
		var expected = { title: photo.title, url: photoURL };
		var actual = FlickrFetcher.transformPhoto(photo);
		actual.should.deep.equal(expected);
	});
});

describe('#fetchFlickrData()', function() {
	it('should take an API key and fetcher function argument and return a promise for JSON data.', function() {
		fakeFetcher.withArgs(expectedFetchURL).returns(Promise.resolve(fakeData));
		return FlickrFetcher.fetchFlickrData(apiKey, fakeFetcher).then(function(actual) {
			expect(actual).to.eql(fakeData);
		});
	});
});

describe('#fetchPhotos()', function() {
	it('should take an API key and fetcher function, and return a promise for transformed photos', function() {
		var expected = [{ title: photo.title, url: photoURL },
						{ title: anotherPhoto.title, url: anotherPhotoURL }];
		fakeFetcher.withArgs(expectedFetchURL).returns(Promise.resolve(fakeData));

		return FlickrFetcher.fetchPhotos(apiKey, fakeFetcher).then(function(actual) {
			actual.should.deep.equal(expected);
		});
	});
});

