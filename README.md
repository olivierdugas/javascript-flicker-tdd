This is my first use of javascript. I followed this [tutorial from jrsinclair.com](https://jrsinclair.com/articles/2016/gentle-introduction-to-javascript-tdd-intro/).

To use this project:

* [Install Nodejs](https://nodejs.org/en/download/package-manager/)
* npm install mocha -g
* npm install chai
* npm install sinon
* npm test \*spec\*

Enjoy!





