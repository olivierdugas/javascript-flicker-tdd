var FlickrFetcher;

FlickrFetcher = {
	photoToURL: function(photo) {
		return ['https://farm', photo.farm, '.staticflickr.com/', photo.server, '/',
				photo.id, '_', photo.secret, '_', photo.size, '.jpg'].join(''); 
	},

	transformPhoto: function(photo) {
		return {
            title: photo.title,
            url:   FlickrFetcher.photoToURL(photo)
        };
	},

	fetchFlickrData: function(apiKey, fetch) {
		var url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key='
				+ apiKey + '&text=pugs&format=json&nojsoncallback=1';
		return fetch(url);
	},

	fetchPhotos: function(apiKey, fetch) {
		return FlickrFetcher.fetchFlickrData(apiKey, fetch).then(function(data) {
			return data.photos.photo.map(FlickrFetcher.transformPhoto);
		});
	}
};

module.exports = FlickrFetcher;
